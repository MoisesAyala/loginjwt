package com.test.loginjwt.core

import android.util.Base64

fun String.getDecodedJwt(): String {
    val result = StringBuilder()
    val parts = this.split("[.]".toRegex()).toTypedArray()
    runCatching {
        var index = 0
        for (part in parts) {
            if (index >= 2) break
            index++
            val decodedBytes: ByteArray =
                Base64.decode(part.toByteArray(charset("UTF-8")), Base64.URL_SAFE)
            result.append(String(decodedBytes, Charsets.UTF_8))
        }
    }.onFailure { throwable ->
        throw RuntimeException("Couldn't decode jwt", throwable)
    }
    return result.toString()
}