package com.test.loginjwt.domain.model.response

data class SessionResponse(
    val success: Boolean,
    val token: String?,
    val msg: String?
)
