package com.test.loginjwt.domain.model

import com.google.gson.annotations.SerializedName

data class JWTData(
    val typ: String,
    val alg: String,
    val titular: String,
    val url: String,
    val email: String,
    @SerializedName("solicitud")
    val request: String
)
