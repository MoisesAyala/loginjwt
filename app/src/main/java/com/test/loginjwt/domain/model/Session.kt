package com.test.loginjwt.domain.model

data class Session(
    val email: String,
    val password: String
)
