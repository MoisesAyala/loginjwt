package com.test.loginjwt.domain.repository

import com.test.loginjwt.domain.model.response.SessionResponse

interface SessionRepository {
    suspend fun getSessionResponse(email: String, password: String): Result<SessionResponse>
}