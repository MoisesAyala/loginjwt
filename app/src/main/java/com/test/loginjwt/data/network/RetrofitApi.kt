package com.test.loginjwt.data.network

import com.test.loginjwt.domain.model.response.SessionResponse
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Field

interface RetrofitApi {

    @FormUrlEncoded
    @POST(".")
    suspend fun getSessionResponse(
        @Field("email") email: String,
        @Field("password") password: String
    ): SessionResponse
}