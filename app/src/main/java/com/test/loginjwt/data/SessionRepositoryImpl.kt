package com.test.loginjwt.data

import com.test.loginjwt.data.network.RetrofitApi
import com.test.loginjwt.data.network.RetrofitClient
import com.test.loginjwt.domain.model.response.SessionResponse
import com.test.loginjwt.domain.repository.SessionRepository

class SessionRepositoryImpl : SessionRepository {

    private val retrofit = RetrofitClient.retrofit.create(RetrofitApi::class.java)

    override suspend fun getSessionResponse(
        email: String,
        password: String
    ): Result<SessionResponse> {
        val result = retrofit.getSessionResponse(email, password)
        return if (!result.token.isNullOrEmpty())
            Result.success(result)
        else Result.failure(Throwable())
    }
}