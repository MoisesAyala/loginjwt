package com.test.loginjwt.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.gson.Gson
import com.test.loginjwt.R
import com.test.loginjwt.core.getDecodedJwt
import com.test.loginjwt.databinding.FragmentResultBinding
import com.test.loginjwt.domain.model.JWTData
import com.test.loginjwt.ui.viewmodel.SessionViewModel


class ResultFragment : Fragment() {

    private var _binding: FragmentResultBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SessionViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = initViewBinding(inflater, container)

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentResultBinding.inflate(inflater, container, ATTACH_TO_PARENT)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.sessionChanged.value?.let { result ->
            result.onSuccess { data ->
                data.token?.let { jwtToken ->
                    serializeDataFromJwt(jwtToken)
                }
            }
        }
    }

    private fun serializeDataFromJwt(jwtToken: String) {
        val result = jwtToken.getDecodedJwt().replace("}{", ",")
        val data = Gson().fromJson(result, JWTData::class.java)
        setDataOnUI(data)
    }

    private fun setDataOnUI(data: JWTData) {
        with(binding) {
            typ.text = getString(R.string.template_typ_data, data.typ)
            alg.text = getString(R.string.template_alg_data, data.alg)
            titular.text = getString(R.string.template_titular_data, data.titular)
            url.text = getString(R.string.template_url_data, data.url)
            email.text = getString(R.string.template_email_data, data.email)
            request.text = getString(R.string.template_request_data, data.request)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        private const val ATTACH_TO_PARENT = false
    }
}