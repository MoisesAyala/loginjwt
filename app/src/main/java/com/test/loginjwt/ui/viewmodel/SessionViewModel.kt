package com.test.loginjwt.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.loginjwt.data.SessionRepositoryImpl
import com.test.loginjwt.domain.model.response.SessionResponse

class SessionViewModel : ViewModel() {

    private val repository = SessionRepositoryImpl()
    var sessionChanged = MutableLiveData<Result<SessionResponse>>()

    suspend fun getSessionResponse(email: String, password: String) {
        sessionChanged.postValue(repository.getSessionResponse(email, password))
    }
}