package com.test.loginjwt.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.test.loginjwt.databinding.FragmentFirstBinding
import com.test.loginjwt.ui.viewmodel.SessionViewModel
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {

    private lateinit var mainActivity: MainActivity

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SessionViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = activity as MainActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = initViewBinding(inflater, container)

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentFirstBinding.inflate(inflater, container, ATTACH_TO_PARENT)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnLogin.setOnClickListener {
            if (emailIsCorrect() && passwordIsCorrect())
                callSession()
            else Toast.makeText(requireContext(), ERROR_MESSAGE, Toast.LENGTH_SHORT).show()
        }
    }

    private fun callSession() {
        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        lifecycleScope.launch {
            viewModel.getSessionResponse(email, password)
        }
        viewModel.sessionChanged.observe(viewLifecycleOwner) { result ->
            result.onSuccess {
                mainActivity.goToResultFragment()
            }
            result.onFailure {
                //restartView()
            }
        }
    }

    private fun emailIsCorrect(): Boolean {
        return binding.etEmail.text.toString() == EMAIL_REQUIRED
    }

    private fun passwordIsCorrect(): Boolean {
        return binding.etPassword.text.toString() == PASSWORD_REQUIRED
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        private const val ATTACH_TO_PARENT = false
        private const val ERROR_MESSAGE = "Por favor, verifica tus datos"
        private const val EMAIL_REQUIRED = "admin@macropay.mx"
        private const val PASSWORD_REQUIRED = "Admin1"
    }
}